#!/bin/bash

SRCDIR=$(dirname "${BASH_SOURCE[0]}")
PYTHON=''
if [ -z "$(which ansible-playbook)" ]; then
    if [ ! -z $(which python3) ] ; then
      PYTHON='python3'
    elif [ ! -z $(which python) ]; then
      PYTHON='python'
    fi
    if [ -z "$PYTHON" ]; then
      echo "No python 2/3 found, exiting"
      exit 1
    fi
    pipver=$($PYTHON -m pip -V | sed 's/^pip \([0-9][0-9]*\).*$/\1/')
    if [ -z "$pipver" ] || [ $pipver -lt 20 ] ; then
      echo "No pip version more then 20.0 found, exiting"
      exit 1
    fi
    PYVER=$($PYTHON -c 'import sys; print(sys.version_info[0])')
    if [ $PYVER = '3' ]; then
      MINOR=$($PYTHON -c 'import sys; print(sys.version_info[1])')
      if [ $MINOR -lt 6 ]; then
          echo "Python 3.$MINOR is unsupported, exiting"
          echo "Use python 2.7 or 3.6+"
          exit 1
      fi
    fi
    $PYTHON -m pip install --no-cache-dir --no-index --find-links=$SRCDIR/pypackages/$PYVER $SRCDIR/pypackages/$PYVER/ansible*.whl
fi
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts.yaml install-agent.yaml install-collectd.yaml
